<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 *
 * @Route("/")
 */
class IndexController extends AbstractController
{
    /**
     * @Route("/weather", name="weather")
     */
    public function index()
    {
        return $this->render('weather.html.twig');
    }
}