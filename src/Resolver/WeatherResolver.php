<?php

namespace App\Resolver;

use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;


final class WeatherResolver implements ResolverInterface, AliasedInterface
{

    /**
     * @return String
     */
    public function resolve(String $city)
    {
        $apiResponse = $this->getWeather($city);
        $weather = new \stdClass();
        $weather->country = $apiResponse->location->country;
        $weather->city = $apiResponse->location->city;
        $weather->condition = $apiResponse->current_observation->condition->text;
        $weather->temperature = $apiResponse->current_observation->condition->temperature;
        $weather->humidity = $apiResponse->current_observation->atmosphere->humidity;
        $weather->wind = $apiResponse->current_observation->wind->speed;
        return $weather;
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'Weather',
        ];
    }

    public function buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    public function buildAuthorizationHeader($oauth) {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach($oauth as $key=>$value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }

    public function getWeather(String $city){
        $url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
        $app_id = 'IHbpi64m';
        $consumer_key = 'dj0yJmk9bGpnUG1tS1RsQ2J4JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTU2';
        $consumer_secret = '6486bea25cb738ed106750d23fa9859582f00604';
        $query = array(
            'location' => $city,
            'format' => 'json',
            'u' => 'c',
        );
        $oauth = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => uniqid(mt_rand(1, 1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );
        $base_info = $this->buildBaseString($url, 'GET', array_merge($query, $oauth));
        $composite_key = rawurlencode($consumer_secret) . '&';
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        $header = array(
            $this->buildAuthorizationHeader($oauth),
            'X-Yahoo-App-Id: ' . $app_id
        );
        $options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url . '?' . http_build_query($query),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        $return_data = json_decode($response);
        return $return_data;
    }
}